package dad.tagger

import dad.tagger.CollisionAnalyst.getMapping
import dad.tagger.Entity.Notion

import scala.collection.mutable

class CollisionAnalyst(conf: Configuration) {

  val termKey2notions = {
    conf.terms
      .map(term =>
        (
          term.index.map(_.wordId).mkString(","),
          conf.notions(term.notionId)
        )
      )
      .groupBy(_._1)
      .mapValues(_.map(_._2))
      .filter(_._2.length > 1)
  }

  val mapping = getMapping(termKey2notions)

  def getNotionParentId(notionId: Int): Int = mapping.notionId2parentId.getOrElse(notionId, notionId)
}

object CollisionAnalyst {

  class MutableMapping(
      val notionId2parentId: mutable.HashMap[Int, Int] = mutable.HashMap.empty,
      val parentId2notionIds: mutable.HashMap[Int, mutable.HashSet[Int]] = mutable.HashMap.empty
  )

  class Mapping(
      val notionId2parentId: Map[Int, Int],
      val parentId2notionIds: Map[Int, Set[Int]]
  )

  def getMapping(termKey2notions: Map[String, Array[Notion]]): Mapping = {
    implicit val mapping = new MutableMapping()

    termKey2notions
      .map(_._2)
      .foreach(notions => {
        for (notion <- notions) {
          if (mapping.notionId2parentId.contains(notion.id)) {
            val curParentId = getNotionParentId(notion.id)
            val termParentId = getNotionParentId(notions.head.id)
            val parentId = if (curParentId > termParentId) termParentId else curParentId
            if (parentId != curParentId) {
              addNotionParent(curParentId, parentId)
            }
            if (parentId != termParentId) {
              addNotionParent(termParentId, parentId)
            }
            addNotionParent(notion.id, parentId)
          } else {
            addNotionParent(notion.id, getNotionParentId(notions.head.id))
          }
        }
      })
    new Mapping(
      mapping.notionId2parentId.toMap,
      mapping.parentId2notionIds.map(e => (e._1, e._2.toSet)).toMap
    )
  }

  private def addNotionParent(notionId: Int, parentId: Int)(implicit mapping: MutableMapping): Unit = {
    if (notionId == parentId) {
      return
    }
    if (!mapping.parentId2notionIds.contains(parentId)) {
      mapping.parentId2notionIds.put(parentId, mutable.HashSet.empty[Int])
    }
    if (mapping.parentId2notionIds.contains(notionId)) {
      for (notionId <- mapping.parentId2notionIds(notionId)) {
        mapping.notionId2parentId.put(notionId, parentId)
        mapping.parentId2notionIds(parentId).add(notionId)
      }
      mapping.parentId2notionIds.remove(notionId)
    }
    mapping.notionId2parentId.put(notionId, parentId)
    mapping.parentId2notionIds(parentId).add(notionId)
  }
  private def getNotionParentId(notionId: Int)(implicit mapping: MutableMapping): Int = mapping.notionId2parentId.getOrElse(notionId, notionId)
}
