package dad.tagger

object Entity {

  case class Notion(id: Int, name: String, terms: Array[Term])

  /**
    *
    * @param words: слова термина
    * @param wordIds: отображение слов термина в id этих слов
    * @param params: параметры термина
    * @param index: отсортированный по id индекс слов
    */
  case class Term(id: Int, notionId: Int, notionTagNum: Int, words: Array[String], wordIds: Array[Int], params: TermParams, index: Array[WordIndex])

  /**
    *
    * @param contact: максимально возможное количество слов в тексте между словами из термина при его выделении
    * @param randomOrder: если true, не учитываем порядок слов
    * @param ignoreRepeat: если true, игнорируем повторяющиеся слова из тега
    */
  case class TermParams(contact: Int = 0, randomOrder: Boolean = false, ignoreRepeat: Boolean = false)
  case class WordIndex(wordId: Int, positions: Array[Int])
  case class SearchResult(termIds: Array[Int], sortedTermWordIds: Array[Int])
  case class MatchResult(termId: Int, textPosFrom: Int, textPosTo: Int)
  case class Tag(words: Array[String], textPosFrom: Int, textPosTo: Int, term: Term, notion: Notion)
}
