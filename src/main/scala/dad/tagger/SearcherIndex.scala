package dad.tagger

import scala.collection.immutable.HashMap
import scala.collection.mutable
import scala.collection.mutable.ArrayBuffer

object SearcherIndex {
  case class Item(wordId: Int, present: Int, termIds: Array[Int], last: Item, var next: Map[Int, Item] = null)

  private case class MutableItem(wordId: Int, present: Int, termIds: ArrayBuffer[Int], next: mutable.HashMap[Int, MutableItem])

  def apply(config: Configuration): Map[Int, Item] = {
    val data = mutable.HashMap.empty[Int, MutableItem]
    for (term <- config.terms) {
      var map = data
      var prevItem: MutableItem = null
      var cnt = 0
      for (termWord <- term.index) {
        cnt += 1
        if (!map.contains(termWord.wordId)) {
          map.put(termWord.wordId, MutableItem(termWord.wordId, cnt, ArrayBuffer.empty, mutable.HashMap.empty))
        }
        prevItem = map(termWord.wordId)
        map = prevItem.next
      }
      prevItem.termIds.append(term.id)
    }
    HashMap(data.mapValues(toItem(null)).toSeq: _*)
  }

  private def toItem(last: Item)(mutableItem: MutableItem): Item = {
    val item = Item(mutableItem.wordId, mutableItem.present, mutableItem.termIds.toArray, last)
    item.next = HashMap(mutableItem.next.mapValues(toItem(item)).toSeq: _*)
    item
  }
}
