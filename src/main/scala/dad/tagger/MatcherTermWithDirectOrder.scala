package dad.tagger

import dad.tagger.Entity.{Term, MatchResult}
import dad.tagger.Matcher.Matching

import scala.collection.mutable.ArrayBuffer

class MatcherTermWithDirectOrder {

  case class Context(term: Term, matchings: Array[Matching], from: Int, var to: Int, var matchCnt: Int = 1, var skipCnt: Int = 0)

  def process(term: Term, matchings: Array[Matching]): Array[MatchResult] = {
    val buffer = ArrayBuffer.empty[MatchResult]
    for (i <- (0 until matchings.length)) {
      val wordId = term.wordIds(matchings(i).termIndexPos)
      if (wordId == term.wordIds.head) {
        val ctx = Context(term, matchings, matchings(i).textPos, matchings(i).textPos + 1)
        if (processMatch(i + 1, ctx)) {
          buffer.append(MatchResult(term.id, ctx.from, ctx.to))
        }
      }
    }
    buffer.toArray
  }

  private def processMatch(i: Int, ctx: Context): Boolean = {
    var termWordPos = 1
    var k = i
    while (termWordPos < ctx.term.wordIds.length) {
      k = findNextIndex(k, ctx.term.wordIds(termWordPos), ctx)
      if (k == -1) {
        return false
      }
      termWordPos += 1
    }
    true
  }

  private def findNextIndex(i: Int, targetWordId: Int, ctx: Context): Int = {
    for (k <- (i until ctx.matchings.length)) {
      val matching = ctx.matchings(k)
      if (matching.textPos - ctx.from - ctx.matchCnt - ctx.skipCnt > ctx.term.params.contact) {
        return -1
      }
      val wordId = ctx.term.wordIds(matching.termIndexPos)
      if (wordId == targetWordId) {
        ctx.matchCnt += 1
        ctx.to = matching.textPos + 1
        return k + 1
      } else if (ctx.term.params.ignoreRepeat) {
        ctx.skipCnt += 1
      }
    }
    -1
  }

}
