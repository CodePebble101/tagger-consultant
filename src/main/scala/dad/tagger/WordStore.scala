package dad.tagger

import dad.tagger.Entity.WordIndex

class WordStore(allWords: Array[String]) {
  private val words = (
    allWords
      .map((_, 1))
      .groupBy(_._1)
      .mapValues(_.map(_._2).reduce(_ + _))
      .toArray
      .sortBy(-_._2)
      .map(_._1)
    )
  private val word2id = words.zipWithIndex.toMap

  def text(id: Int) = words(id)

  def getWordIds(text: Array[String]): Array[Int] = text.map(word2id)

  def getIndex(text: Array[String]): Array[WordIndex] = {
    text.zipWithIndex
      .map { case (word, pos) => (word2id.getOrElse(word.toUpperCase, -1), pos) }
      .groupBy(_._1)
      .map { case (wordId, values) => WordIndex(wordId, values.map(_._2)) }
      .filter(_.wordId >= 0)
      .toArray
      .sortBy(_.wordId)
  }
}
