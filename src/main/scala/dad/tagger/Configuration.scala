package dad.tagger

import dad.tagger.Entity.{Notion, Term, TermParams}

import java.io.File
import scala.collection.mutable.ArrayBuffer
import scala.io.Source

/**
  *
  * @param terms
  * для каждого id тега храним написание
  * @param notions
  * для каждого id понятия храним список id тегов
  * @param words
  * для каждого id слова храним написание
  */
case class Configuration(
    notions: Array[Notion],
    terms: Array[Term],
    words: WordStore
)
object Configuration {
  type ParsedWords = Array[String]
  type ParsedTerm = (ParsedWords, TermParams)
  type ParsedNotion = Array[ParsedTerm]
  type ParsedNotions = Array[ParsedNotion]

  def load(file: File): Unit = {
    val parsedConfig = {
      val source = Source.fromFile(file, "utf-8")
      try {
        source
          .getLines()
          .map(_.split("==").map(parseTermInfo))
          .toArray
      } finally {
        source.close()
      }
    }
    load(parsedConfig)
  }

  def load(parsedConfig: Array[Array[String]]): Configuration = {
    load(parsedConfig.map(_.map(parseTermInfo)))
  }

  private def load(parsedConfig: ParsedNotions): Configuration = {
    val wordStore = new WordStore(parsedConfig.flatten.map(_._1).flatten)
    val terms = ArrayBuffer.empty[Term]
    val notions = {
      parsedConfig.zipWithIndex
        .map {
          case (parserNotion, notionId) =>
            Notion(
              notionId,
              parserNotion.head._1.mkString(" "),
              parserNotion.zipWithIndex
                .map {
                  case ((words, params), notionTermNum) => {
                    val wordIds = wordStore.getWordIds(words)
                    val index = wordStore.getIndex(words)
                    terms.append(Term(terms.size, notionId, notionTermNum, words, wordIds, params, index))
                    terms.last
                  }
                }
            )
        }
    }
    Configuration(notions, terms.toArray, wordStore)
  }

  private def parseTermInfo(text: String): (Array[String], TermParams) = {
    val parts = text.split("\t").map(_.trim())
    val words = parts.last.split(" ").map(_.trim().toUpperCase)
    val params = if (parts.length > 1) parts.head.split(",") else Array.empty[String]
    val contact = parseIntParam(params, "contact", 0)
    val randomOrder = params.exists(_ == "randomOrder")
    val ignoreRepeat = params.exists(_ == "ignoreRepeat")
    (words, TermParams(contact, randomOrder, ignoreRepeat))
  }

  private def parseIntParam(params: Array[String], name: String, defaultValue: Int): Int = {
    params.find(_.startsWith(name + "=")).map(_.split("=").last.trim.toInt).getOrElse(defaultValue)
  }
}
