package dad.tagger

import dad.tagger.Entity.{Term, Notion, Tag}
import org.scalatest.FunSuite

class TaggerTest extends FunSuite {

  val config = Configuration.load(
    Array(
      Array(
        "АВАНС АРЕНДА",
        "АВАНС ПО ДОГОВОРУ АРЕНДЫ",
        "АВАНСОВЫЕ ПЛАТЕЖИ ПО ДОГОВОРУ АРЕНДЫ",
        "АВАНСОВЫЙ ПЛАТЕЖ ПО АРЕНДЕ",
        "АРЕНДНАЯ ПЛАТА АВАНСОМ",
        "randomOrder\tДОГОВОР АРЕНДЫ ПРЕДОПЛАТА",
        "ДОГОВОР АРЕНДЫ С АВАНСОМ"
      ),
      Array(
        "contact=1\tАВАНС ДОГОВОР ПОСТАВКИ",
        "АВАНС ПОСТАВКА",
        "ПРЕДВАРИТЕЛЬНАЯ ОПЛАТА ТОВАРА",
        "ПРЕДВАРИТЕЛЬНО ОПЛАЧЕННЫЙ ТОВАР",
        "ПРЕДОПЛАТА ДОГОВОР ПОСТАВКИ",
        "ПРЕДОПЛАТА ЗА ТОВАР"
      ),
      Array(
        "АВАНС ЗЕМЕЛЬНЫЙ НАЛОГ",
        "АВАНСОВЫЕ ПЛАТЕЖИ ЗЕМЕЛЬНЫЙ НАЛОГ",
        "АВАНСОВЫЕ ПЛАТЕЖИ ПО НАЛОГУ НА ЗЕМЛЮ"
      ),
      Array(
        "АВАНС",
        "АВАНС АРЕНДА",
        "АВАНС ДОГОВОР ПОСТАВКИ",
        "АВАНС ЗЕМЕЛЬНЫЙ НАЛОГ"
      )
    )
  )

  test("Пример интеграционного теста для Tagger") {
    val tagger = new Tagger(config)
    val text = "внести аванс за договор поставки предоплата договор аренды".split(" ")
    val result = tagger.process(text)
    assert(
      result === Array(
        Tag(text, 1, 2, config.terms(16), notion("АВАНС")),
        Tag(text, 5, 8, config.terms(5), notion("АВАНС АРЕНДА")),
        Tag(text, 1, 5, config.terms(7), notion("АВАНС ДОГОВОР ПОСТАВКИ"))
      )
    )
  }

  test("Пример интеграционного теста для CollisionAnalyst") {
    val collisionAnalyst = new CollisionAnalyst(config)
    assert(
      collisionAnalyst.termKey2notions.mapValues(_.toList) === Map(
        wordsKey("АВАНС ДОГОВОР ПОСТАВКИ") -> Array(notion("АВАНС ДОГОВОР ПОСТАВКИ"), notion("АВАНС")),
        wordsKey("АВАНС АРЕНДА") -> Array(notion("АВАНС АРЕНДА"), notion("АВАНС")),
        wordsKey("АВАНС ЗЕМЕЛЬНЫЙ НАЛОГ") -> Array(notion("АВАНС ЗЕМЕЛЬНЫЙ НАЛОГ"), notion("АВАНС"))
      ).mapValues(_.toList)
    )
    assert(
      collisionAnalyst.mapping.notionId2parentId === Map(
        notion("АВАНС ЗЕМЕЛЬНЫЙ НАЛОГ").id -> notion("АВАНС АРЕНДА").id,
        notion("АВАНС ДОГОВОР ПОСТАВКИ").id -> notion("АВАНС АРЕНДА").id,
        notion("АВАНС").id -> notion("АВАНС АРЕНДА").id
      )
    )
    assert(
      collisionAnalyst.mapping.parentId2notionIds === Map(
        notion("АВАНС АРЕНДА").id -> Set(notion("АВАНС ДОГОВОР ПОСТАВКИ").id, notion("АВАНС ЗЕМЕЛЬНЫЙ НАЛОГ").id, notion("АВАНС").id)
      )
    )
  }

  def notion(name: String): Notion = {
    config.notions.find(_.name == name).orNull
  }

  def wordsKey(text: String): String = {
    config.words
      .getIndex(text.split(" "))
      .map(_.wordId)
      .mkString(",")
  }
}
